from behave import *
import subprocess

@given(u'I am in the current working directory')
def step_impl(context):
    pwd = subprocess.run('pwd', stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True, text=True)

@when(u'I run \'ls\'')
def step_impl(context):
    ls = subprocess.run('ls', stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True, text=True)


@then(u'the terminal prints the contents of the current working directory')
def step_impl(context):
    print("daaaang")
