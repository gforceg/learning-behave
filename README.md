# learning-behave

behave is a python BDD tool

1. install it:
  ```bash
  sudo pip install behave
  ```

2. running behave provides a clue as to where to put the feature files

```bash
behave
ConfigError: No steps directory in '/home/greg/src/learning-behave/features'
```

3. add a feature file:

```bash
mkdir -p features/steps
echo """
Feature: listing files

Scenario: listing files in the current directory
Given I am in the current working directory
When I run 'ls'
Then the terminal prints the contents of the current working directory
""" > features/list-files.feature
```

4. add some steps: (created features/steps/list-files.steps.py)

